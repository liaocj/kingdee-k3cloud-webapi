# kingdee-k3cloud-webapi

#### 介绍
最近工作需要对接金蝶云，逛了一番金蝶社区一直没找到对应的java sdk，算了就自己写一个吧，也许对时间紧张难得动手的同学有所帮助，并且sdk已经内置了登录功能，只要提供相应的账号密码即可。基于金蝶云的webapi抽出了[K3CloudTemplate](https://gitee.com/phenixs/kingdee-k3cloud-webapi/blob/master/src/main/java/com/link/kingdee/k3cloud/K3CloudTemplate.java)，简化操作。但是目前主要提供了[表单数据查询服务](https://gitee.com/phenixs/kingdee-k3cloud-webapi/blob/master/src/main/java/com/link/kingdee/k3cloud/domain/RequestService.java)，[表单保存服务](https://gitee.com/phenixs/kingdee-k3cloud-webapi/blob/master/src/main/java/com/link/kingdee/k3cloud/domain/RequestService.java)，[表单提交服务](https://gitee.com/phenixs/kingdee-k3cloud-webapi/blob/master/src/main/java/com/link/kingdee/k3cloud/domain/RequestService.java)，有需求的同学可以fork后自己进行改造。

#### 软件架构
无特殊架构，只是一个简单的jar，但是需要基于java8+及spring环境，具体使用的框架大致如下：
| 框架         | 描述     |
|------------|--------|
| java       | java8+ |
| spring-web | 主要使用了RestTemplate进行http请求|
| lombok     | 对象简化工具|
| hutool     | java实用工具集|



#### 安装教程

- 源码安装 

  `git clone xxxx`

- jar安装

  `mvn clean package -Dmaven.test.skip=true`

#### 使用说明
- 需要替换对应的账套id，用户名，密码及对应的url

```java
package com.link.kingdee;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.link.kingdee.k3cloud.K3CloudTemplate;
import com.link.kingdee.k3cloud.domain.Authentication;
import com.link.kingdee.k3cloud.domain.BillQuery;
import com.link.kingdee.k3cloud.domain.BillSave;
import com.link.kingdee.k3cloud.domain.BillSubmit;

import junit.framework.TestCase;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AppTest extends TestCase {
    
    private static final Authentication AUTH = new Authentication("${actId}", "${username}", "${password}");
    private static final K3CloudTemplate TEMPLATE = new K3CloudTemplate("${url}", AUTH);
    
    /**
     * 单据查询
     */
    @Test
    public void testBillQuery() {
        // 查询字段
        String[] fieldKeys = {
                  "FBOOKID","FNumber","FName"
                };
        // 过滤条件, 具体格式可以参考金蝶文档, 比如:"FBOOKID=100453"
        String filter = "";
        BillQuery query = new BillQuery("BD_AccountBook", StringUtils.arrayToDelimitedString(fieldKeys, ","), filter);
        // 允许最大查询量, 金蝶默认最大限制好像是2000, 超过这个值设置了也没用
        // query.setTopRowCount(2000);
        // 起始索引, 0代表第一个, 需要配合limit使用才有效果
        query.setStartRow(0);
        // 0代表无限制
        query.setLimit(0);
        List<?> dataList = TEMPLATE.executeBillQuery(query, AccountBook.class);
        log.info("AccountBookList = {}", dataList);
        assertNotNull(dataList);
    }
    
    /**
     * 单据保存
     */
    @Test
    public void testBillSave() {
        BillSave billSave = new BillSave("BD_MATERIAL");
        Map<String, Object> model = new HashMap<>();
        // 传入id就是更新
        // model.put("FMATERIALID", 142631);
        Map<String, Object> crtOrgMap = new HashMap<>();
        crtOrgMap.put("FNumber", "102");
        model.put("FCreateOrgId", crtOrgMap);
        Map<String, Object> grpMap = new HashMap<>();
        model.put("FName", "物料测试");
        grpMap.put("FNumber", "ig001");
        model.put("FMaterialGroup", grpMap);

        Map<String, Object> subHeadMap = new HashMap<>();
        Map<String, Object> baseUnitMap = new HashMap<>();
        baseUnitMap.put("FNumber", "01");
        subHeadMap.put("FBaseUnitId", baseUnitMap);
        model.put("SubHeadEntity", subHeadMap);

        Map<String, Object> taxRateMap = new HashMap<>();
        taxRateMap.put("FNumber", "SL02_SYS");
        model.put("FTaxRateId", taxRateMap);

        billSave.setModel(model);
        Result result = TEMPLATE.executeBillSave(billSave, Result.class);
        log.info("result={}", result.get("Result"));
    }
    
    /**
     * 单据提交
     */
    @Test
    public void testBillSubmit() {
        BillSubmit<String> billSubmit = new BillSubmit<>("BD_MATERIAL");
        billSubmit.setNumbers(Arrays.asList("0021"));
        Result object = TEMPLATE.executeBillSubmit(billSubmit, Result.class);
        log.info("result={}", object);
    }
    
    

    /**
     * 自定义账簿类用于接收返回数据
     * <p>使用jackson @JsonProperty来对应金蝶返回字段</p>
     */
    @Data
    public static class AccountBook {

        @JsonProperty("FBOOKID")
        private Integer bookId;

        @JsonProperty("FNumber")
        private Integer number;

        @JsonProperty("FName")
        private String name;

    }
    
    public static class Result extends HashMap<String, Object> {

        private static final long serialVersionUID = 1L;
        
    }
    
}

```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
