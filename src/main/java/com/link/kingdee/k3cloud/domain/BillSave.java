package com.link.kingdee.k3cloud.domain;

import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 单据保存
 * @author dong.tang
 *
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class BillSave extends BillEnity {
	
	/**
	 * 对应接口文档中的model
	 */
	private Map<String, Object> model;
	
	public BillSave(String formId) {
		super(formId);
	}
	
}
