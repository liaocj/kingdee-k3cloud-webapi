package com.link.kingdee.k3cloud.domain;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class BillSubmit<T> extends BillEnity {

    private Integer createOrgId;

    private Integer useOrgId;

    private List<T> numbers;

    public BillSubmit(String formId) {
        super(formId);
    }

}
