package com.link.kingdee.k3cloud.domain;

public enum RequestService {
	
	SERVICE_AUTH("Kingdee.BOS.WebApi.ServicesStub.AuthService.ValidateUser.common.kdsvc", "登陆验证服务"),
	SERVICE_EXCUTE_OPERATION("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.ExcuteOperation.common.kdsvc",""),
	SERVICE_SAVE("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Save.common.kdsvc", "保存表单数据服务"),
	SERVICE_BATCH_SAVE("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.BatchSave.common.kdsvc", "批量保存表单数据服务"),
	SERVICE_AUDIT("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Audit.common.kdsvc", "审核表单数据服务"),
	SERVICE_DELETE("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Delete.common.kdsvc", "删除表单数据服务"),
	SERVICE_SUBMIT("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Submit.common.kdsvc", "提交表单数据服务"),
	SERVICE_VIEW("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.View.common.kdsvc", "查看表单数据服务"),
	SERVICE_EXECUTE_BILL_QUERY("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.ExecuteBillQuery.common.kdsvc", "表单数据查询服务"),
	SERVICE_DRAFT("Kingdee.BOS.WebApi.ServicesStub.DynamicFormService.Draft.common.kdsvc", "暂存表单数据服务");
	
	private String name;
	
	private String comment;
	
	RequestService(String name, String comment) {
		this.name = name;
		this.comment = comment;
	}
	
	public String getName() {
		return name;
	}
	
	public String getComment() {
		return comment;
	}
	
}
