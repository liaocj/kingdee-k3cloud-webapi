package com.link.kingdee;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.link.kingdee.k3cloud.K3CloudTemplate;
import com.link.kingdee.k3cloud.domain.Authentication;
import com.link.kingdee.k3cloud.domain.BillQuery;
import com.link.kingdee.k3cloud.domain.BillSave;
import com.link.kingdee.k3cloud.domain.BillSubmit;

import junit.framework.TestCase;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AppTest extends TestCase {
	
	private static final Authentication AUTH = new Authentication("${actId}", "${username}", "${password}");
	private static final K3CloudTemplate TEMPLATE = new K3CloudTemplate("${url}", AUTH);
	
	/**
	 * 单据查询
	 */
	@Test
	public void testBillQuery() {
		// 查询字段
		String[] fieldKeys = {
			      "FBOOKID","FNumber","FName"
			    };
		// 过滤条件, 具体格式可以参考金蝶文档, 比如:"FBOOKID=100453"
	    String filter = "";
	    BillQuery query = new BillQuery("BD_AccountBook", StringUtils.arrayToDelimitedString(fieldKeys, ","), filter);
	    // 允许最大查询量, 金蝶默认最大限制好像是2000, 超过这个值设置了也没用
	    // query.setTopRowCount(2000);
	    // 起始索引, 0代表第一个, 需要配合limit使用才有效果
	    query.setStartRow(0);
	    // 0代表无限制
	    query.setLimit(0);
		List<?> dataList = TEMPLATE.executeBillQuery(query, AccountBook.class);
		log.info("AccountBookList = {}", dataList);
		assertNotNull(dataList);
	}
	
	/**
	 * 单据保存
	 */
	@Test
	public void testBillSave() {
		BillSave billSave = new BillSave("BD_MATERIAL");
		Map<String, Object> model = new HashMap<>();
		// 传入id就是更新
        // model.put("FMATERIALID", 142631);
        Map<String, Object> crtOrgMap = new HashMap<>();
        crtOrgMap.put("FNumber", "102");
        model.put("FCreateOrgId", crtOrgMap);
        Map<String, Object> grpMap = new HashMap<>();
        model.put("FName", "物料测试");
        grpMap.put("FNumber", "ig001");
        model.put("FMaterialGroup", grpMap);

        Map<String, Object> subHeadMap = new HashMap<>();
        Map<String, Object> baseUnitMap = new HashMap<>();
        baseUnitMap.put("FNumber", "01");
        subHeadMap.put("FBaseUnitId", baseUnitMap);
        model.put("SubHeadEntity", subHeadMap);

        Map<String, Object> taxRateMap = new HashMap<>();
        taxRateMap.put("FNumber", "SL02_SYS");
        model.put("FTaxRateId", taxRateMap);

        billSave.setModel(model);
        Result result = TEMPLATE.executeBillSave(billSave, Result.class);
        log.info("result={}", result.get("Result"));
	}
	
	/**
	 * 单据提交
	 */
	@Test
	public void testBillSubmit() {
		BillSubmit<String> billSubmit = new BillSubmit<>("BD_MATERIAL");
		billSubmit.setNumbers(Arrays.asList("0021"));
		Result object = TEMPLATE.executeBillSubmit(billSubmit, Result.class);
		log.info("result={}", object);
	}
	
	

	/**
	 * 自定义账簿类用于接收返回数据
	 * <p>使用jackson @JsonProperty来对应金蝶返回字段</p>
	 */
	@Data
	public static class AccountBook {

		@JsonProperty("FBOOKID")
		private Integer bookId;

		@JsonProperty("FNumber")
		private Integer number;

		@JsonProperty("FName")
		private String name;

	}
	
	public static class Result extends HashMap<String, Object> {

		private static final long serialVersionUID = 1L;
		
	}
	
}
